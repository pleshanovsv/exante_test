package impl

import scala.annotation.tailrec
import scala.io.StdIn.readLine
import scala.util.Try

/** Необходимо реализовать метод solution, который на вход получает два интеджера A и B и возвращает максимальное
 * количество повторяющихся квадратных корней, которые могут выполнены с использованием чисел, которые находятся внутри
 * интервала [A,B] (включая оба конца). Извлечение квадратного корня продолжается до тех пор, пока результат
 * целочисленный.
 *
 * 1. Например для значений A = 10 и B = 20 функция должна вернуть 2, т.к. начиная с числа 16 могут быть выполнены 2
 * операции извлечения квадратного корня с 16 -> 4 -> 2.
 * 2. Например для значений A = 6000 и B = 7000 функция должна вернуть 3, т.к. 6561 -> 81 -> 9 -> 3
 *
 * Необходимо реализовать эффективный алгоритм имея ввиду следующие ограничения:
 * 1. A и B - это Int в интервале [2, 1 000 000 000]
 * 2. A <= B
 */

object Solution {

  def main(args: Array[String]): Unit = {
    println("Set A and B values in two lines of input.")
    val range = for {
      a <- Try(Integer.parseInt(readLine())).toOption
      b <- Try(Integer.parseInt(readLine())).toOption
    } yield (a, b)

    range match {
      case Some((a, b)) if a < 2 || a > 1000000000 || b < 2 || b > 1000000000 =>
        println("A or B are not in the range [2, 1 000 000 000]")
      case Some((a, b)) if a > b => println("A is greater than B")
      case Some((a, b)) if a <= b =>
        println(s"Maximum number of repeated square root in range [$a, $b] is ${getSolution(a, b)}")
      case None => println("Invalid parameters given")
    }
  }

  @tailrec
  def getSolution(a: Int, b: Int, acc: Int = 0): Int = {
    val aRoot = math.ceil(math.sqrt(a)).toInt
    val bRoot = math.floor(math.sqrt(b)).toInt
    if (aRoot > bRoot) acc else getSolution(aRoot, bRoot, acc + 1)
  }

}
