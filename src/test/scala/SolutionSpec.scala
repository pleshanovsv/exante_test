import impl.Solution
import org.scalatest.{FunSpec, Matchers}

class SolutionSpec extends FunSpec with Matchers {

  it("should return 2 if input range is [10, 20]") {
    Solution.getSolution(a = 10, b = 20) shouldBe 2
  }

  it("should return 3 if input range is [6000, 7000]") {
    Solution.getSolution(a = 6000, b = 7000) shouldBe 3
  }

  it("should return 4 if input range is [2, 1000000000]") {
    Solution.getSolution(a = 2, b = 1000000000) shouldBe 4
  }

  it("should return 0 if A greater B") {
    Solution.getSolution(a = 20, b = 10) shouldBe 0
  }

}
